﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using ICSI.DAL.EF;
using ICSI.DAL.Models;
using ICSI.DAL.Models.Login;
using ICSI.Repository;
using ICSI.Repository.Login;
using ICSI.Web.Models;
//using ICSI.Model;

namespace ICSI.Web.Controllers
{
    public class AccountController : Controller
    {
        private LoginRepository loginRepository = new LoginRepository();
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Session["allMenudIdsForRoleUser"] != null && Session["CurrentUser"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                if (Session["TempFrgtMsg"] != null)
                {
                    TempData["isForgotPwdSuccess"] = true;
                    Session["TempFrgtMsg"] = null;
                }
                Session["isForgotPwdSuccess"] = null;
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string url)
        {
            if(ModelState.IsValid)
            {
                if(model.Email!=null && model.Password!=null)
                {
                    ModelState.Clear();
                    Session["CurrentUser"] = null;

                    var flag = loginRepository.ValidateUser(model.Email, model.Password);
                    if(flag)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                        var loginUser = loginRepository.GetUserDetails(model.Email);
                        if (loginUser != null && loginUser.IsActive == 1)
                        {
                            UserRoleMaster userRole = loginRepository.GetUserRoleDetails(loginUser.UserID);
                            List<UserRoleMaster> roleMasters = loginRepository.GetRoleMasterDetails(loginUser.UserID);
                            if (userRole == null || userRole.IsActive == false)
                            {
                                return RedirectToAction("Unauthorized");
                            }
                            else
                            {
                                Session["UserName"] = loginUser.Email;
                                CurrentUserDetail currentUserDetail = null;
                                currentUserDetail = new CurrentUserDetail();
                                currentUserDetail.UserID = loginUser.UserID;
                                currentUserDetail.RegistrationNumber = loginUser.RegistrationNumber;
                                currentUserDetail.FirstName = loginUser.FirstName;
                                currentUserDetail.MiddleName = loginUser.MiddleName;
                                currentUserDetail.LastName = loginUser.LastName;
                                currentUserDetail.FullName = loginUser.FirstName + " " + loginUser.MiddleName + " " + loginUser.LastName;
                                currentUserDetail.Email = loginUser.Email;
                                currentUserDetail.ModifiedDate = loginUser.ModifiedDate;
                                currentUserDetail.CreatedDate = loginUser.CreatedDate;
                                currentUserDetail.IsActive = loginUser.IsActive;
                                currentUserDetail.UserTypeID = loginUser.UserTypeID;
                                List<MenuMaster> menusList = loginRepository.GetMenuDetailsByUserId(loginUser.UserID);
                                currentUserDetail.RoleMaster = userRole;
                                currentUserDetail.MultiRole = roleMasters;
                                currentUserDetail.MenuMaster = menusList;
                                Session["CurrentUser"] = currentUserDetail;
                                Session["UserRole"] = userRole;
                                Session["MultiRole"] = roleMasters;

                                return RedirectToAction("Index","Home");
                            }
                        }
                        else
                        {
                                ModelState.AddModelError("ErrorMessage", "User is locked, please contact administrator.");
                                return View(model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ErrorMessage", "Invalid username or password.");
                    }
                }
                else
                {
                    ModelState.AddModelError("ErrorMessage", "Invalid username or password.");
                }
            }
            return View(model);
        }
    }
}