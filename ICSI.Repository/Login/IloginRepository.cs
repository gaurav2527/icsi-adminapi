﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSI.Repository.Login
{
    public interface ILoginRepository
    {
        Boolean ValidateUser(String userName, String password);
    }
}
