﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSI.DAL.EF;
using ICSI.DAL.Models;
using ICSI.DAL.Models.Login;

namespace ICSI.Repository.Login
{
    public class LoginRepository
    {
        //private ICSI_APPDEVEntities dbContext;
        public bool ValidateUser(string email, string password)
        {
            var flag = false;
            try
            {
                //var encryptedPassword = password.ToMD5HashForPassword();
                using (var dbContext = new ICSI_APPDEVEntities())
                {
                    //var user = dbContext.UmUserMasters.FirstOrDefault(cond => cond.UserName == userName && cond.Password == encryptedPassword);
                    var user = dbContext.UserLoginMasters.FirstOrDefault(cond => cond.Email == email && cond.Password == password);
                    if (user != null)
                    {
                        flag = true;
                    }
                }
                
            }
            catch(Exception ex)
            {

            }
            return flag;
        }

        public UserLoginMaster GetUserDetails(string userName)
        {
            UserLoginMaster userMaster = null;
            using (var dbContext = new ICSI_APPDEVEntities())
            {
                userMaster = dbContext.UserLoginMasters.FirstOrDefault(cond => cond.Email == userName);

            }
            return userMaster;
        }

        public UserRoleMaster GetUserRoleDetails(int? UserRefId)
        {
            using (var dbContext = new ICSI_APPDEVEntities())
            {
                UserRoleMaster userRole = null;
                userRole = (from roles in dbContext.UserMasterRoles
                            join roleMaster in dbContext.UserRoleMasters on roles.RoleId equals roleMaster.RoleId
                            where roles.UserID == UserRefId && roles.IsActive == 1
                            select roleMaster).FirstOrDefault();

                return userRole;
            }
        }

        public List<UserRoleMaster> GetRoleMasterDetails(int UserRefId)
        {
            using (var dbContext = new ICSI_APPDEVEntities())
            {
                var roless = (from roles in dbContext.UserMasterRoles
                              join roleMaster in dbContext.UserRoleMasters on roles.RoleId equals roleMaster.RoleId
                              where roles.UserID == UserRefId
                              select roleMaster).ToList();
                return roless;
            }
        }

        public List<MenuMaster> GetMenuDetailsByUserId(int userId)
        {
            List<MenuMaster> menuList = null;
            using (var dbContext = new ICSI_APPDEVEntities())
            {
                menuList = (from menu in dbContext.UserMenuMasters
                            join rmt in dbContext.UserRoleMasterTrans on menu.MenuId equals rmt.MenuId
                            join rat in dbContext.UserMasterRoles on rmt.RoleId equals rat.RoleId
                            where rat.UserID == userId
                            select new MenuMaster
                            {
                                MenuId = menu.MenuId,
                                MenuName = menu.MenuName,
                                MenuPathId = menu.MenuPathId,
                                MenuParentId = menu.MenuParentId,
                                IsActive = menu.IsActive,
                                IsRead = rmt.IsRead,
                                IsWrite = rmt.IsWrite,
                                IsUpdate = rmt.IsUpdate,
                                Isdelete = rmt.Isdelete
                            }).ToList();
            }
            return menuList;
        }
    }
}
