﻿using ICSI.DAL;
using ICSI.DAL.EF;
using ICSI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSI.Repository.MenuMapping
{
    public class MenuMappingRepository : IMenuMappingRepository
    {
        public List<GetMenuID> GetMenuIdsByRoleID(List<int> roleIds)
        {
            var menuIds = new List<GetMenuID>();
            using (var context = new ICSI_APPDEVEntities())
            {
                menuIds = (from umRoleMasterTrans in context.UserRoleMasterTrans
                           join dd in context.UserMenuMasters on umRoleMasterTrans.MenuId equals dd.MenuId
                           where roleIds.Contains(umRoleMasterTrans.RoleId)
                            && dd.MenuParentId != null
                           select new GetMenuID
                           {
                               menuID = umRoleMasterTrans.MenuId,
                               parentID = dd.MenuParentId.Value
                           }).ToList();
            }
            return menuIds;
        }

        public IEnumerable<Roles> GetRoles(string roleType)
        {
            var result = new List<Roles>();
            using (var dbContext = new ICSI_APPDEVEntities())
            {
                if (roleType.ToLower() == Constants.SuperAdmin.ToLower())
                {
                    result = (from lstRoles in dbContext.UserRoleMasters.ToList()
                              where lstRoles.RoleType.ToLower() == Constants.Admin.ToLower() && lstRoles.IsActive == true
                              select new Roles
                              {
                                  roleId = lstRoles.RoleId,
                                  roleName = lstRoles.RoleName,
                                  roleDescription = lstRoles.RoleDescription,
                                  roleType = lstRoles.RoleType
                              }).ToList();
                }
                else if (roleType.ToLower() == Constants.Admin.ToLower())
                {
                    result = (from lstRoles in dbContext.UserRoleMasters.ToList()
                              where lstRoles.RoleType.ToLower() != Constants.Admin.ToLower() && lstRoles.RoleType.ToLower() != Constants.SuperAdmin.ToLower()
                              && lstRoles.IsActive == true
                              select new Roles
                              {
                                  roleId = lstRoles.RoleId,
                                  roleName = lstRoles.RoleName,
                                  roleDescription = lstRoles.RoleDescription,
                                  roleType = lstRoles.RoleType
                              }).ToList();
                }
            }
            return result;
        }
    }
}
