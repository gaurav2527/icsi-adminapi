﻿using ICSI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSI.Repository.MenuMapping
{
    public interface IMenuMappingRepository
    {
        IEnumerable<Roles> GetRoles(string roleType);
        List<GetMenuID> GetMenuIdsByRoleID(List<int> roleIds);
    }
}
