﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSI.DAL
{
    public static class Constants
    {
        public const string SuperAdmin = "SA";
        public const string Admin = "A";
        public const string User = "U";
        public const string Consultant = "Consultant";
        public const string Male = "Male";
        public const string Female = "Female";
        public const string Company = "Company";
        public const string Individual = "Individual";
        public const string Married = "married";
        public const string DD = "DD";
        public const string RTGS = "RTGS";
        public const string Other = "Other";
    }
}
