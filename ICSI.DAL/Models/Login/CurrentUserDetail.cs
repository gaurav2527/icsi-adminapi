﻿using ICSI.DAL.EF;
using ICSI.DAL.Models.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSI.DAL.Models
{
    public class CurrentUserDetail
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string RegistrationNumber { get; set; }
        public string Password { get; set; }
        public string MobileNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int IsActive { get; set; }
        public int DepartmentID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string FullName { get; set; }
        public int UserTypeID { get; set; }
        public UserRoleMaster RoleMaster { get; set; }
        public List<UserRoleMaster> MultiRole { get; set; }
        public List<MenuMaster> MenuMaster { get; set; }
        public Nullable<int> RoleId { get; set; }

    }
}
